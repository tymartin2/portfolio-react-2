import { Navbar, Nav, Container } from 'react-bootstrap'

export default function NavBar() {
    return (
      <div>
           <Navbar bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand href="#home">Martin Miguel</Navbar.Brand>
                    <Nav className="me-auto">
                    <Nav.Link href="#home">Home</Nav.Link>
                    <Nav.Link href="#features">About</Nav.Link>
                    <Nav.Link href="#pricing">Work</Nav.Link>
                    </Nav>
                </Container>
             </Navbar>
      </div>
    );
  }
  

  